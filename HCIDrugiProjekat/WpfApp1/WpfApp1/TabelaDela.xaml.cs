﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for TabelaDela.xaml
    /// </summary>
    public partial class TabelaDela : Window
    {
		public MainWindow window { get; set; }
		public ObservableCollection<Delo> Dela { get; set; }
		public HashSet<Delo> pomocna = new HashSet<Delo>();

		public TabelaDela()
        {
            InitializeComponent();
        }

		public TabelaDela(MainWindow mw)
		{
			InitializeComponent();
			DataContext = this;
			this.window = mw;
			Dela = new ObservableCollection<Delo>();
			Dela = this.window.Dela;
		}

		private void ButtonSearch_Click(object sender, RoutedEventArgs e)
		{
			pomocna.Clear();

			for (int index = 0; index < Dela.Count(); index++)
			{
				if (NazivDela.Text != "")
				{
					if (NazivDela.Text.ToUpper() == Dela[index].Naziv.ToUpper())
					{
						pomocna.Add(Dela[index]);
					}

				}

				if (MestoDela.Text != "")
				{
					if (MestoDela.Text.ToUpper() == Dela[index].Mesto.ToUpper())
					{
						pomocna.Add(Dela[index]);
					}

				}

				if (GodinaIzgradnjeDela.Text != "")
				{
					if (GodinaIzgradnjeDela.Text == Dela[index].GodinaIzgradnje)
					{
						pomocna.Add(Dela[index]);
					}

				}

				Epoha epohas;
				if (CBEpoha.Text != "")
				{
					switch (CBEpoha.Text)
					{
						case "Drevna Mesopotamija":
							epohas = WpfApp1.Epoha.drevnaMesopotamija;
							break;
						case "Antika":
							epohas = WpfApp1.Epoha.antika;
							break;
						case "Vizantija":
							epohas = WpfApp1.Epoha.vizantija;
							break;
						case "Ranohriscanstvo":
							epohas = WpfApp1.Epoha.ranohriscanstvo;
							break;
						case "Romanika":
							epohas = WpfApp1.Epoha.romanika;
							break;
						case "Gotika":
							epohas = WpfApp1.Epoha.gotika;
							break;
						case "Renesansa":
							epohas = WpfApp1.Epoha.renesansa;
							break;
						case "Barok":
							epohas = WpfApp1.Epoha.barok;
							break;
						case "Klasicizam":
							epohas = WpfApp1.Epoha.klasicizam;
							break;
						case "Moderna":
							epohas = WpfApp1.Epoha.moderna;
							break;
						case "Postmoderna":
							epohas = WpfApp1.Epoha.postmoderna;
							break;
						default: epohas = WpfApp1.Epoha.antika; break;
					}

					if (epohas == Dela[index].Epoha)
					{
						pomocna.Add(Dela[index]);
					}
				}

				bool SC;
				if (checkBoxSCDa.IsChecked==true && checkBoxSCNe.IsChecked==false)
				{
					SC = true;
					if (SC == Dela[index].SvetskoCudo)
					{
						pomocna.Add(Dela[index]);
					}
				}
				else if (checkBoxSCDa.IsChecked == false  && checkBoxSCNe.IsChecked == true)
				{
					SC = false;
					if (SC == Dela[index].SvetskoCudo)
					{
						pomocna.Add(Dela[index]);
					}
				}

				bool zas;
				if (checkBoxZDa.IsChecked == true && checkBoxZNe.IsChecked == false)
				{
					zas = true;
					if (zas == Dela[index].Zasticeno)
					{
						pomocna.Add(Dela[index]);
					}
				}
				else if (checkBoxZDa.IsChecked == false && checkBoxZNe.IsChecked == true)
				{
					zas = false;
					if (zas == Dela[index].Zasticeno)
					{
						pomocna.Add(Dela[index]);
					}
				}


			}
			//resourcesGrid.SetBinding(pomocna);
			Dela.Clear();

			foreach (Delo d in pomocna)
			{
				Dela.Add(d);
			}
		}

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string str = "tabelaDela";
            HelpProvider.ShowHelp(str, window);
        }
    }
}
