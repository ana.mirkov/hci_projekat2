﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.IO;
using System.Collections.ObjectModel;

namespace WpfApp1
{
	/// <summary>
	/// Interaction logic for DodajDelo.xaml
	/// </summary>
	public partial class DodajDelo : Window
	{
		string path1 = @"..\..\Podaci\dela1.txt";
		public string IconPath { get; set; }
		public MainWindow Window { get; set; }

		public ObservableCollection<Arhitekta> arhitekte { get; set; }

		public DodajDelo()
		{
			IconPath = "";
			InitializeComponent();
		}

		public DodajDelo(MainWindow mw)
		{
			DataContext = this;
			arhitekte = mw.Arhitekte;
			IconPath = "";
			InitializeComponent();
			this.Window = mw;
		}

		private void BtnAddDelo_Click(object sender, RoutedEventArgs e)
		{
			Epoha epohas = WpfApp1.Epoha.antika;
			switch (Epoha.Text)
			{
				case "Drevna Mesopotamija":
					epohas = WpfApp1.Epoha.drevnaMesopotamija;
					break;
				case "Antika":
					epohas = WpfApp1.Epoha.antika;
					break;
				case "Vizantija":
					epohas = WpfApp1.Epoha.vizantija;
					break;
				case "Ranohriscanstvo":
					epohas = WpfApp1.Epoha.ranohriscanstvo;
					break;
				case "Romanika":
					epohas = WpfApp1.Epoha.romanika;
					break;
				case "Gotika":
					epohas = WpfApp1.Epoha.gotika;
					break;
				case "Renesansa":
					epohas = WpfApp1.Epoha.renesansa;
					break;
				case "Barok":
					epohas = WpfApp1.Epoha.barok;
					break;
				case "Klasicizam":
					epohas = WpfApp1.Epoha.klasicizam;
					break;
				case "Moderna":
					epohas = WpfApp1.Epoha.moderna;
					break;
				case "Postmoderna":
					epohas = WpfApp1.Epoha.postmoderna;
					break;
			}

			string proverenaPutanja = proveriPutanju();

			Delo d = null;
			try {
				d = new Delo(Naziv.Text, Mesto.Text, Opis.Text, GodinaIzgradnje.Text, epohas, SvetskoCudo.IsChecked ?? false, Zasticen.IsChecked ?? false, Namena.Text, proverenaPutanja);
			}
			catch
			{
				System.Media.SystemSounds.Beep.Play();
				MessageBox.Show("Neke vrednosti nisu dobro unete");
				return;
			}

			//MainWindow mw = new MainWindow();
			Window._dela.Add(d);
			string json = JsonConvert.SerializeObject(Window._dela, Formatting.Indented);

			using (StreamWriter sw = File.CreateText(path1))
			{
				sw.WriteLine(json);
			}
            this.Close();
        }

		public string proveriPutanju()
		{
			if (IconPath.Equals(""))
			{
				return "..\\..\\Mape\\ikonicaDelo.png";
			}
			else
			{
				return IconPath;
			}
		}

		private void BtnAddIkonica_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();

			dlg.DefaultExt = ".png";
			dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";

			Nullable<bool> result = dlg.ShowDialog();

			if (result == true)
			{
				string sourcePath = dlg.FileName;
				//string targetPath = "../../Data/" + Id.Text + "." + sourcePath.Split('.')[1];
				//System.IO.File.Copy(sourcePath, targetPath, true);
				IconPath = sourcePath; //targetPath;
			}

		}

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string str = "dodajDelo";
            HelpProvider.ShowHelp(str, Window);
        }
    }
}
