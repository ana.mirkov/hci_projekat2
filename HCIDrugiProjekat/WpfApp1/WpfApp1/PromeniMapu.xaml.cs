﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
	/// <summary>
	/// Interaction logic for PromeniMapu.xaml
	/// </summary>
	public partial class PromeniMapu : Window
	{
		public MainWindow window { get; set; }

		public PromeniMapu()
		{
			InitializeComponent();
		}

		public PromeniMapu(MainWindow mw)
		{
			InitializeComponent();
			this.window = mw;
		}

		private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			string path = "..\\..\\Mape\\world_map.png";

			ImageBrush imgB = new ImageBrush();
			imgB.ImageSource = new System.Windows.Media.Imaging.BitmapImage(new Uri(path, UriKind.Relative));

			window.Map.Background = imgB;
			this.Close();

		}

		private void ListViewItem_MouseDoubleClick_1(object sender, MouseButtonEventArgs e)
		{
			string path = "..\\..\\Mape\\europe_map.jpg";

			ImageBrush imgB = new ImageBrush();
			imgB.ImageSource = new System.Windows.Media.Imaging.BitmapImage(new Uri(path, UriKind.Relative));

			window.Map.Background = imgB;
			this.Close();
		}

		private void ListViewItem_MouseDoubleClick_2(object sender, MouseButtonEventArgs e)
		{
			string path = "..\\..\\Mape\\asia_map.jpg";

			ImageBrush imgB = new ImageBrush();
			imgB.ImageSource = new System.Windows.Media.Imaging.BitmapImage(new Uri(path, UriKind.Relative));

			window.Map.Background = imgB;
			this.Close();
		}

		private void ListViewItem_MouseDoubleClick_3(object sender, MouseButtonEventArgs e)
		{
			string path = "..\\..\\Mape\\south_america_map.jpg";

			ImageBrush imgB = new ImageBrush();
			imgB.ImageSource = new System.Windows.Media.Imaging.BitmapImage(new Uri(path, UriKind.Relative));

			window.Map.Background = imgB;
			this.Close();
		}

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string str = "promeniMapu";
            HelpProvider.ShowHelp(str, window);
        }
    }
}
