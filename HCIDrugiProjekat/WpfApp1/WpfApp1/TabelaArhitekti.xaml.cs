﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for TabelaArhitekti.xaml
    /// </summary>
    public partial class TabelaArhitekti : Window
    {
		public MainWindow window { get; set; }
		//public ObservableCollection<Arhitekta> _arhitekte;
		public ObservableCollection<Arhitekta> Arhitekte { get; set; }
		public HashSet<Arhitekta> pomocna = new HashSet<Arhitekta>();

		public TabelaArhitekti()
        {
            InitializeComponent();
        }

		public TabelaArhitekti(MainWindow mw)
		{
			InitializeComponent();
			DataContext = this;
			this.window = mw;
			Arhitekte = new ObservableCollection<Arhitekta>();
			Arhitekte = this.window.Arhitekte;
		}

		private void ButtonSearch_Click(object sender, RoutedEventArgs e)
		{
			pomocna.Clear();

			for (int index = 0; index < Arhitekte.Count(); index++)
			{
				if (IdArhitekte.Text != "")
				{
					if (IdArhitekte.Text == Arhitekte[index].IdArhitekte)
					{
						pomocna.Add(Arhitekte[index]);
					}

				}

				if (ImeArhitekte.Text != "")
				{
					if (ImeArhitekte.Text.ToUpper() == Arhitekte[index].Name.ToUpper())
					{
						pomocna.Add(Arhitekte[index]);
					}
				}

				if (PrezimeArhitekte.Text != "")
				{
					if (PrezimeArhitekte.Text.ToUpper() == Arhitekte[index].Surname.ToUpper())
					{
						pomocna.Add(Arhitekte[index]);
					}
				}

				if (MestoRodjenja.Text != "")
				{
					if (MestoRodjenja.Text.ToUpper() == Arhitekte[index].MestoRodjenja.ToUpper())
					{
						pomocna.Add(Arhitekte[index]);
					}
				}

				if (NacionalnostArhitekte.Text != "")
				{
					if (NacionalnostArhitekte.Text.ToUpper() == Arhitekte[index].Nacionalnost.ToUpper())
					{
						pomocna.Add(Arhitekte[index]);
					}
				}
			}
			//resourcesGrid.SetBinding(pomocna);
			Arhitekte.Clear();

			foreach (Arhitekta a in pomocna)
			{
				Arhitekte.Add(a);
			}
		}

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string str = "tabelaArhitekti";
            HelpProvider.ShowHelp(str, window);
        }
    }
}
