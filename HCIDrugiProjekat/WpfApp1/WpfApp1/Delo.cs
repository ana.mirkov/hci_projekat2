﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
	public enum Epoha { praistorijskaArhitektura, drevnaMesopotamija, antika, vizantija, ranohriscanstvo, romanika, gotika, renesansa, barok, klasicizam, moderna, postmoderna }

	public class Delo : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string name)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}

		private string naziv;
		private string mesto;
		private string opis;
		private string godinaIzgradnje;
		private List<Arhitekta> arhitekte = new List<Arhitekta>();
		private Epoha epoha;
		private bool svetskoCudo;
		private bool zasticeno;
		private string namena;
		private string ikonica;
		private Dictionary<string, int> xPoint;
		private Dictionary<string, int> yPoint;


		public string Naziv { get => naziv; set => naziv = value; }
		public string Mesto { get => mesto; set => mesto = value; }
		public string Opis { get => opis; set => opis = value; }
		public string GodinaIzgradnje { get => godinaIzgradnje; set => godinaIzgradnje = value; }
		public List<Arhitekta> Arhitekte { get => arhitekte; set => arhitekte = value; }
		public Epoha Epoha { get => epoha; set => epoha = value; }
		public bool SvetskoCudo { get => svetskoCudo; set => svetskoCudo = value; }
		public bool Zasticeno { get => zasticeno; set => zasticeno = value; }
		public string Namena { get => namena; set => namena = value; }
		public string Ikonica { get => ikonica; set => ikonica = value; }
		public Dictionary<string, int> XPoint { get => xPoint; set => xPoint = value; }
		public Dictionary<string, int> YPoint { get => yPoint; set => yPoint = value; }

		public Delo(string n, string m, string o, string gi, Epoha e, bool sc, bool z, string na, string i)
		{
			naziv = n;
			mesto = m;
			opis = o;
			godinaIzgradnje = gi;
			epoha = e;
			svetskoCudo = sc;
			zasticeno = z;
			namena = na;
			ikonica = i;
            arhitekte = new List<Arhitekta>();

		}

	}
}
