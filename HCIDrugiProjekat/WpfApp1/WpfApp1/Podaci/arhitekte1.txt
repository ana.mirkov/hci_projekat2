[
  {
    "Slicica": null,
    "Filter": false,
    "Name": "Pera",
    "Surname": "Peric",
    "IdArhitekte": "1",
    "DatumRodjenja": "1979-02-02T00:00:00",
    "DatumSmrti": "2020-02-02T00:00:00",
    "MestoRodjenja": "Madjarska",
    "Nacionalnost": "Madjar",
    "SlikaArhitekte": "..\\..\\Mape\\arhitekta.png",
    "XPoint": null,
    "YPoint": null,
    "ImePrezime": ""
  },
  {
    "Slicica": null,
    "Filter": false,
    "Name": "Mika",
    "Surname": "Mikic",
    "IdArhitekte": "2",
    "DatumRodjenja": "1979-03-03T00:00:00",
    "DatumSmrti": "2020-03-03T00:00:00",
    "MestoRodjenja": "Rumunija",
    "Nacionalnost": "Rumun",
    "SlikaArhitekte": "..\\..\\Mape\\arhitekta.png",
    "XPoint": null,
    "YPoint": null,
    "ImePrezime": ""
  },
  {
    "Slicica": null,
    "Filter": false,
    "Name": "Josh",
    "Surname": "Johny",
    "IdArhitekte": "3",
    "DatumRodjenja": "2020-06-14T11:56:36.717734+02:00",
    "DatumSmrti": "2020-06-14T11:56:36.717734+02:00",
    "MestoRodjenja": "Engleska",
    "Nacionalnost": "Englez",
    "SlikaArhitekte": "..\\..\\Mape\\arhitekta.png",
    "XPoint": null,
    "YPoint": null,
    "ImePrezime": ""
  },
  {
    "Slicica": null,
    "Filter": false,
    "Name": "Bogdan",
    "Surname": "Bogdanovic",
    "IdArhitekte": "4",
    "DatumRodjenja": "2020-06-14T12:05:40.4846298+02:00",
    "DatumSmrti": "2020-06-14T12:05:40.4846298+02:00",
    "MestoRodjenja": "Srbija",
    "Nacionalnost": "Srbin",
    "SlikaArhitekte": "..\\..\\Mape\\arhitekta.png",
    "XPoint": null,
    "YPoint": null,
    "ImePrezime": ""
  },
  {
    "Slicica": null,
    "Filter": false,
    "Name": "Milos",
    "Surname": "Nikolic",
    "IdArhitekte": "5",
    "DatumRodjenja": "1919-09-09T00:00:00",
    "DatumSmrti": "2015-08-16T00:00:00",
    "MestoRodjenja": "Pariz",
    "Nacionalnost": "Francuz",
    "SlikaArhitekte": "..\\..\\Mape\\arhitekta.png",
    "XPoint": null,
    "YPoint": null,
    "ImePrezime": "MilosNikolic"
  }
]
