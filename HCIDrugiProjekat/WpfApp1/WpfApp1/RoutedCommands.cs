﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp1
{
    public class RoutedCommands
    {
        public static RoutedUICommand PromeniMapu = new RoutedUICommand(
           "Otvara prozor za odabir mape",
           "PromeniMapu",
           typeof(RoutedCommand),
           new InputGestureCollection()
           {
                new KeyGesture(Key.M,ModifierKeys.Control)
           }
           );

        public static RoutedUICommand PrebaciPodatke = new RoutedUICommand(
           "Otvara prozor za prebacivanje podataka sa mape na mapu",
           "PrebaciPodatke",
           typeof(RoutedCommand),
           new InputGestureCollection()
           {
                new KeyGesture(Key.M, ModifierKeys.Control | ModifierKeys.Shift)
           }
           );
    }
}
