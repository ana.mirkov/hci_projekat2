﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.IO;
namespace WpfApp1
{
	/// <summary>
	/// Interaction logic for IzmenaArhitekte.xaml
	/// </summary>
	public partial class IzmenaArhitekte : Window
	{
        string path1 = @"..\..\Podaci\arhitekte1.txt";

        private int itemIndex;
        public String IconPath { get; set; }
        public String ActiveMap { get; set; }
        public MainWindow Window { get; set; }

        public IzmenaArhitekte()
        {
            InitializeComponent();
        }



        public IzmenaArhitekte(MainWindow window, int index)
		{
            Arhitekta arh = window.Arhitekte[index];
            this.itemIndex = index;
           
            this.ActiveMap = window.ActiveMap;
            this.Window = window;
            InitializeComponent();
            IconPath = "";
            Id.Text = arh.IdArhitekte;
            ImeArhitekte.Text = arh.Name;
            PrezimeArhitekte.Text = arh.Surname;
            DatumRodjenja.SelectedDate = arh.DatumRodjenja;
            DatumSmrti.SelectedDate = arh.DatumSmrti;
            MestoRodjenja.Text = arh.MestoRodjenja;
            Nacionalnost.Text = arh.Nacionalnost;
        }

        //izmena arhitekte
        private void BtnUpdateFile_Click(object sender, RoutedEventArgs e)
        {
            Arhitekta arh = this.Window.Arhitekte[this.itemIndex];
            arh.IdArhitekte = Id.Text;
            arh.Name = ImeArhitekte.Text;
            arh.Surname = PrezimeArhitekte.Text;
            arh.DatumRodjenja = DatumRodjenja.SelectedDate.GetValueOrDefault(DateTime.Now);
            arh.DatumSmrti= DatumSmrti.SelectedDate.GetValueOrDefault(DateTime.Now);
            arh.MestoRodjenja = MestoRodjenja.Text;
            arh.Nacionalnost = Nacionalnost.Text;
            //ikonicaaaaaaaaaaaaaaaaaa
            string json = JsonConvert.SerializeObject(this.Window.Arhitekte, Formatting.Indented);

            using (StreamWriter sw = File.CreateText(path1))
            {
                sw.WriteLine(json);
            }
            this.Close();
        }

        private void BtnDeleteFile_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Da li ste sigurni da zelite da obrisete ovog arhitektu?", "Provera brisanja", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                this.Window.Arhitekte.RemoveAt(this.itemIndex);
                string json = JsonConvert.SerializeObject(this.Window.Arhitekte, Formatting.Indented);

                using (StreamWriter sw = File.CreateText(path1))
                {
                    sw.WriteLine(json);
                }
                this.Close();
            }
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string str = "izmenaArhitekte";
            HelpProvider.ShowHelp(str, Window);
        }
    }
}
