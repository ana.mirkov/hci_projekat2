﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.IO;

namespace WpfApp1
{
	/// <summary>
	/// Interaction logic for DodajArhitektu.xaml
	/// </summary>
	public partial class DodajArhitektu : Window
	{
		string path1 = @"..\..\Podaci\arhitekte1.txt";
		public string IconPath { get; set; }
		public MainWindow Window { get; set; }

		public DodajArhitektu()
		{
			IconPath = "";
			InitializeComponent();
		}

		public DodajArhitektu(MainWindow mw)
		{
			IconPath = "";
			InitializeComponent();
			this.Window = mw;
		}

		private void BtnAddArhitektu_Click(object sender, RoutedEventArgs e)
		{
			string proverenaPutanja = proveriPutanju();

			Arhitekta a = null;
			try
			{
				a = new Arhitekta(Id.Text, ImeArhitekte.Text, PrezimeArhitekte.Text, DatumRodjenja.SelectedDate.GetValueOrDefault(DateTime.Now), DatumSmrti.SelectedDate.GetValueOrDefault(DateTime.Now), MestoRodjenja.Text, Nacionalnost.Text, proverenaPutanja);
			}
			catch
			{
				System.Media.SystemSounds.Beep.Play();
				MessageBox.Show("Neke vrednosti nisu dobro unete");
				return;
			}

			//MainWindow mw = new MainWindow();
			Window._arhitekte.Add(a);
			string json = JsonConvert.SerializeObject(Window._arhitekte, Formatting.Indented);

			using (StreamWriter sw = File.CreateText(path1))
			{
				sw.WriteLine(json);
			}
            this.Close();
        }

		


		public string proveriPutanju()
		{
			if (IconPath.Equals(""))
			{
				return "..\\..\\Mape\\arhitekta.png"; 
			}
			else
			{
				return IconPath;
			}
		}

		private void BtnOpenFile_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();

			dlg.DefaultExt = ".png";
			dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";

			Nullable<bool> result = dlg.ShowDialog();

			if (result == true)
			{
				string sourcePath = dlg.FileName;
				//string targetPath = "../../Data/" + Id.Text + "." + sourcePath.Split('.')[1];
				//System.IO.File.Copy(sourcePath, targetPath, true);
				IconPath = sourcePath; //targetPath;
			}
		}

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
           
                string str = "dodajArhitektu";
                HelpProvider.ShowHelp(str, Window);
            
        }
    }
}
