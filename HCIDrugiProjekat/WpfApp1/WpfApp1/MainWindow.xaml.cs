﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.IO;

using System.Drawing;
using System.ComponentModel;
using WpfApp1.Demo;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Arhitekta> _arhitekte;
        private HashSet<Arhitekta> _arhitekte2;

        public ObservableCollection<Delo> _dela;
        private ObservableCollection<Delo> _dela2;


        public String ActiveMap { get; set; }
        Point startPoint = new Point();
        private const int picSize = 20;
        private Arhitekta selectedResource;
        public Arhitekta SelectedResource
        {
            get { return selectedResource; }
            set
            {
                if (value != selectedResource)
                {
                    selectedResource = value;
                    OnPropertyChanged("SelectedResource");
                }
            }
        }

		private Delo selectedResourceDelo;
		public Delo SelectedResourceDelo
		{
			get { return selectedResourceDelo; }
			set
			{
				if (value != selectedResourceDelo)
				{
					selectedResourceDelo = value;
					OnPropertyChanged("SelectedResourceDelo");
				}
			}
		}

		public MainWindow()
        {
            ActiveMap = "word_map.png";
            InitializeComponent();
            this.DataContext = this;

			Map.Background = new ImageBrush(new BitmapImage(new Uri("..\\..\\Mape\\world_map.png", UriKind.Relative)));

			//string path = @"D:\DOKUMENTI\hci_projekat2\HCIDrugiProjekat\WpfApp1\arhitekte.txt"; //C:\Users\Ana\Desktop\hci_projekat2\HCIDrugiProjekat\WpfApp1\arhitekte.txt
			string pathIzmena = @"..\..\Podaci\arhitekte1.txt";
            string path1 = @"..\..\Podaci\dela1.txt";

            List<Arhitekta> a = new List<Arhitekta>();

            using (StreamReader r = new StreamReader(pathIzmena))
            {
                string json = r.ReadToEnd();
                a = JsonConvert.DeserializeObject<List<Arhitekta>>(json);
				r.Close();
            }
            /*Arhitekta a1 = new Arhitekta { IdArhitekte = "1", Name = "Jova", Surname = "Jovic", DatumRodjenja = "01.01.1979", DatumSmrti = "01.01.2020", MestoRodjenja = "Srbija", Nacionalnost = "srbin", SlikaArhitekte = "" };
			Arhitekta a2 = new Arhitekta { IdArhitekte = "2", Name = "Pera", Surname = "Peric", DatumRodjenja = "02.02.1979", DatumSmrti = "02.02.2020", MestoRodjenja = "Madjarska", Nacionalnost = "madjar", SlikaArhitekte = "" };
			Arhitekta a3 = new Arhitekta { IdArhitekte = "3", Name = "Mika", Surname = "Mikic", DatumRodjenja = "03.03.1979", DatumSmrti = "03.03.2020", MestoRodjenja = "Rumunija", Nacionalnost = "rumun", SlikaArhitekte = "" };
			a.Add(a1);
			a.Add(a2);
			a.Add(a3);*/

           /* foreach (Arhitekta ar in a)
			{
				string json = JsonConvert.SerializeObject(ar, Formatting.Indented);

					using (StreamWriter sw = File.AppendText(path))
					{
						sw.WriteLine(json);
					}
					
			}*/

            Arhitekte = new ObservableCollection<Arhitekta>(a);
            Arhitekte2 = new HashSet<Arhitekta>();



            /*List<Arhitekta> ad = new List<Arhitekta>();
			ad.Add(a1);
			ad.Add(a2);*/
            List<Delo> d = new List<Delo>();

            using (StreamReader r = new StreamReader(path1))
            {
                string json = r.ReadToEnd();
                d = JsonConvert.DeserializeObject<List<Delo>>(json);
				r.Close();
            }
           /*Delo d1 = new Delo { Naziv = "delo1", Mesto = "mesto1", Opis = "opis1", GodinaIzgradnje = "", Arhitekte = ad, Epoha = Epoha.antika, SvetskoCudo = true, Zasticeno = true, Namena = "namena1", Ikonica = "C:\\Users\\Ana\\Desktop\\hci_projekat2\\HCIDrugiProjekat\\WpfApp1\\WpfApp1\\Mape\\ikonicaDelo.png" };
			Delo d2 = new Delo { Naziv = "delo2", Mesto = "mesto2", Opis = "opis2", GodinaIzgradnje = "", Arhitekte = ad, Epoha = Epoha.drevnaMesopotamija, SvetskoCudo = false, Zasticeno = false, Namena = "namena2", Ikonica = "C:\\Users\\Ana\\Desktop\\hci_projekat2\\HCIDrugiProjekat\\WpfApp1\\WpfApp1\\Mape\\ikonicaDelo.png" };
			Delo d3 = new Delo { Naziv = "delo3", Mesto = "mesto3", Opis = "opis3", GodinaIzgradnje = "", Arhitekte = ad, Epoha = Epoha.gotika, SvetskoCudo = true, Zasticeno = true, Namena = "namena3", Ikonica = "C:\\Users\\Ana\\Desktop\\hci_projekat2\\HCIDrugiProjekat\\WpfApp1\\WpfApp1\\Mape\\ikonicaDelo.png" };
			d.Add(d1);
			d.Add(d2);
			d.Add(d3);

          
				string json = JsonConvert.SerializeObject(d, Formatting.Indented);

				using (StreamWriter sw = File.AppendText(path1))
				{
					sw.WriteLine(json);
				}*/

			

            Dela = new ObservableCollection<Delo>(d);
            Dela2 = new ObservableCollection<Delo>();

        }

        public ObservableCollection<Arhitekta> Arhitekte { get => _arhitekte; set => _arhitekte = value; }
        public HashSet<Arhitekta> Arhitekte2 { get => _arhitekte2; set => _arhitekte2 = value; }
        public ObservableCollection<Delo> Dela { get => _dela; set => _dela = value; }
        public ObservableCollection<Delo> Dela2 { get => _dela2; set => _dela2 = value; }


        private void ListView_MouseMove(object sender, MouseEventArgs e)
        {
            Point currentPos = e.GetPosition(null);
            Vector v = currentPos - startPoint;

            if (e.LeftButton == MouseButtonState.Pressed && (Math.Abs(v.X) > SystemParameters.MinimumHorizontalDragDistance && Math.Abs(v.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                ListView listView = sender as ListView;

                ListViewItem lvi = FindAncestor<ListViewItem>((DependencyObject)e.OriginalSource);

                Arhitekta a = (Arhitekta)listView.ItemContainerGenerator.ItemFromContainer(lvi);

                DataObject data = new DataObject("arhitekta", a);

                DragDrop.DoDragDrop(lvi, data, DragDropEffects.Copy);
            }
        }

        private static T FindAncestor<T>(DependencyObject current) where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }

                current = VisualTreeHelper.GetParent(current);
            } while (current != null);

            return null;
        }

        private void ListView_Drop(object sender, DragEventArgs e)
        {
            Arhitekta a = e.Data.GetData("arhitekta") as Arhitekta;
            //Arhitekte.Remove(a);
            Arhitekte2.Add(a);
        }

        private void ListView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }

        private void ListView_DragEnter(object sender, DragEventArgs e)
        {
            Arhitekta a = e.Data.GetData("arhitekta") as Arhitekta;

            if (a.MestoRodjenja == "Srbija")
            {
                a.MestoRodjenja = "SRB";
            }
        }
        private WriteableBitmap writeableBitmap;
        //dodavanje arhitekti
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            DodajArhitektu da = new DodajArhitektu(this);
            da.Show();
        }

        //dodavanje dela
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            DodajDelo dd = new DodajDelo(this);
            dd.Show();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            PromeniMapu pm = new PromeniMapu(this);
            pm.Show();
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            PrebaciPodatke pp = new PrebaciPodatke();
            pp.Show();
        }

        private void ListView_MouseMove_1(object sender, MouseEventArgs e)
        {
			Point currentPos = e.GetPosition(null);
			Vector v = currentPos - startPoint;

			if (e.LeftButton == MouseButtonState.Pressed && (Math.Abs(v.X) > SystemParameters.MinimumHorizontalDragDistance && Math.Abs(v.Y) > SystemParameters.MinimumVerticalDragDistance))
			{
				ListView listView = sender as ListView;

				ListViewItem lvi = FindAncestor<ListViewItem>((DependencyObject)e.OriginalSource);

				Delo d = (Delo)listView.ItemContainerGenerator.ItemFromContainer(lvi);

				DataObject data = new DataObject("delo", d);

				DragDrop.DoDragDrop(lvi, data, DragDropEffects.Copy);
			}

		}

        private void ListView_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
        }
        private void MyImage_MouseMove(object sender, MouseEventArgs e)
        {
            Point mousePos = e.GetPosition(Map);
            Vector diff = startPoint - mousePos;

            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
				if (listaArhitekti.SelectedItems.Count > 0)
				{
                    //foreach (Arhitekta arh in this.Arhitekte)
                    //{
                    //if (arh.XPoint[this.ActiveMap] - 20 < startPoint.X && arh.YPoint[this.ActiveMap] + 20 > startPoint.Y)
                    //{
                    //if (arh.XPoint[this.ActiveMap] + 20 > startPoint.X && arh.YPoint[this.ActiveMap] - 20 < startPoint.Y)
                    //{
                    Map.Children.Clear();                   
                    Arhitekta arh = Arhitekta_Click((int)mousePos.X, (int)mousePos.Y);
                    Arhitekte2.Remove(arh);
                    DataObject dragData = new DataObject("arhitekta", arh);
                    DragDrop.DoDragDrop((DependencyObject)e.OriginalSource, dragData, DragDropEffects.Move);
								//break;
							//}
						//}
					//}
				}
				else if (listaDela.SelectedItems.Count > 0) {
                    //foreach (Delo de in this.Dela)
                    //{
                    //	if (de.XPoint[this.ActiveMap] - 20 < startPoint.X && de.YPoint[this.ActiveMap] + 20 > startPoint.Y)
                    //	{
                    //		if (de.XPoint[this.ActiveMap] + 20 > startPoint.X && de.YPoint[this.ActiveMap] - 20 < startPoint.Y)
                    //		{
                    Map.Children.Clear();
                    Delo de = Dela_Click((int)mousePos.X, (int)mousePos.Y);
                    Dela2.Remove(de);
                    DataObject dragData = new DataObject("delo", de);
					DragDrop.DoDragDrop((DependencyObject)e.OriginalSource, dragData, DragDropEffects.Move);
								//break;
					//		}
					//	}
					//}
				}


			}

        }
        private Arhitekta Resource_Click(int x, int y)
        {
            foreach (Arhitekta arh in Arhitekte)
            {
                if (arh.XPoint[this.ActiveMap] != -1 && arh.YPoint[this.ActiveMap] != -1)
                {
                    if (Math.Sqrt(Math.Pow((x - arh.XPoint[this.ActiveMap] - 15), 2) + Math.Pow((y - arh.YPoint[this.ActiveMap] - 15), 2)) < 20)
                    {
                        return arh;
                    }
                }
            }
            return null;

        }

        private Arhitekta Arhitekta_Click(int x, int y)
        {
            foreach (Arhitekta arh in Arhitekte)
            {
                if (arh.XPoint[this.ActiveMap] != -1 && arh.YPoint[this.ActiveMap] != -1)
                {
                    if (Math.Sqrt(Math.Pow((x - arh.XPoint[this.ActiveMap] - 15), 2) + Math.Pow((y - arh.YPoint[this.ActiveMap] - 15), 2)) < 20)
                    {
                        return arh;
                    }
                }
            }
            return null;

        }

        private Delo Dela_Click(int x, int y)
        {
            foreach (Delo d in Dela)
            {
                if (d.XPoint[this.ActiveMap] != -1 && d.YPoint[this.ActiveMap] != -1)
                {
                    if (Math.Sqrt(Math.Pow((x - d.XPoint[this.ActiveMap] - 15), 2) + Math.Pow((y - d.YPoint[this.ActiveMap] - 15), 2)) < 20)
                    {
                        return d;
                    }
                }
            }
            return null;

        }

        private void MyImage_Drop(object sender, DragEventArgs e)
        {
            Point myPoint = e.GetPosition(Map);
            int x = Convert.ToInt32(myPoint.X);
            int y = Convert.ToInt32(myPoint.Y);
            bool f = false;
            
            bool flag = false;
			//Arhitekta arh = e.Data.GetData("arhitekta") as Arhitekta;

			/*foreach (Arhitekta ar in this.Arhitekte)
            {
                if (ar.IdArhitekte == arh.IdArhitekte)
                {
                    continue;
                }
                
            }*/
			if (e.Data.GetDataPresent("arhitekta"))
			{

				Arhitekta arh = e.Data.GetData("arhitekta") as Arhitekta;
				if (!flag)
				{
					
                    if (!Arhitekte2.Contains(arh))
                    {

                        Dictionary<string, int> d = new Dictionary<string, int>();
                        d.Add(this.ActiveMap, x);

                        Dictionary<string, int> d1 = new Dictionary<string, int>();
                        d1.Add(this.ActiveMap, y);
                        arh.XPoint = d;
                        arh.YPoint = d1;

                        Arhitekte2.Add(arh);
                        foreach(Arhitekta arh1 in Arhitekte2)
                        {
                            this.renderMap(arh1.XPoint[this.ActiveMap], arh1.YPoint[this.ActiveMap], arh1);
                        }
                        foreach (Delo arh1 in Dela2)
                        {
                            this.renderMapDelo(arh1.XPoint[this.ActiveMap], arh1.YPoint[this.ActiveMap], arh1);
                        }
                    }
                }
			}
			else if (listaDela.SelectedItems.Count > 0)
			{
				Delo arh = e.Data.GetData("delo") as Delo;
				if (!flag)
				{
					

                    if (!Dela2.Contains(arh))
                    {

                        Dictionary<string, int> d = new Dictionary<string, int>();
                        d.Add(this.ActiveMap, x);

                        Dictionary<string, int> d1 = new Dictionary<string, int>();
                        d1.Add(this.ActiveMap, y);
                        arh.XPoint = d;
                        arh.YPoint = d1;

                        Dela2.Add(arh);
                        foreach(Delo arh1 in Dela2)
                        {
                            this.renderMapDelo(arh1.XPoint[this.ActiveMap], arh1.YPoint[this.ActiveMap], arh1);
                        }
                        foreach (Arhitekta arh1 in Arhitekte2)
                        {
                            this.renderMap(arh1.XPoint[this.ActiveMap], arh1.YPoint[this.ActiveMap], arh1);
                        }
                    }

                }
			}





			}
    
    public void renderMap(int x, int y, Arhitekta ar)
        {
      
            
                string im = ar.SlikaArhitekte;
             
                Image ResourceIcon = new Image();
                ResourceIcon.Width = 30;
                ResourceIcon.Height = 30;
                ResourceIcon.ToolTip = "Id arhitekte: " + ar.IdArhitekte + "\nZa vise informacija pritisnite desni klik misa ";

                if (File.Exists(im))
                {
                 
                    ResourceIcon.Source = new BitmapImage(new Uri(im, UriKind.RelativeOrAbsolute));
                    Map.Children.Add(ResourceIcon);
                    Canvas.SetLeft(ResourceIcon, x);
                    Canvas.SetTop(ResourceIcon, y);
                }
                else
                {
                   /*OVDE IDU INICIJALI ARHITEKTE AKO NEMA SLIKE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
                }

             
            
           
        }

		public void renderMapDelo(int x, int y, Delo ar)
		{

			
				string im = ar.Ikonica;

				Image ResourceIcon = new Image();
				ResourceIcon.Width = 30;
				ResourceIcon.Height = 30;
				ResourceIcon.ToolTip = "Naziv dela: " + ar.Naziv + "\nZa vise informacija pritisnite desni klik misa ";

				if (File.Exists(im))
				{

					ResourceIcon.Source = new BitmapImage(new Uri(im, UriKind.RelativeOrAbsolute));
					Map.Children.Add(ResourceIcon);
					Canvas.SetLeft(ResourceIcon, x);
					Canvas.SetTop(ResourceIcon, y);
				}
				else
				{
					/*OVDE IDU INICIJALI ARHITEKTE AKO NEMA SLIKE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
				}


			

		}

		public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void MyImage_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("arhitekta") || sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }else if (!e.Data.GetDataPresent("delo") || sender == e.Source)
			{
				e.Effects = DragDropEffects.None;
			}

		}



        private bool check_Colision(int x1, int y1, int x2, int y2)
        {
            if (x1 - 20 > x2 + 20 || x2 - 20 > x1 + 20)
            {
                return false;
            }
            if (y1 + 20 < y2 - 20 || y2 + 20 < y1 - 20)
            {
                return false;
            }
            return true;
        }

       /* private void ListaArhitekti_MouseUp(object sender, MouseButtonEventArgs e)
        {
            for (int i = 0; i < this.Arhitekte.Count; i++)
            {
                Arhitekta arh = this.Arhitekte[i];
                if (arh.XPoint[this.ActiveMap] - picSize < startPoint.X && arh.YPoint[this.ActiveMap] + picSize > startPoint.Y)
                {
                    if (arh.XPoint[this.ActiveMap] + picSize > startPoint.X && arh.YPoint[this.ActiveMap] - picSize < startPoint.Y)
                    {
                        IzmenaArhitekte izmenaArhitekte = new IzmenaArhitekte(this, i);
                        izmenaArhitekte.Show();
                        break;
                    }
                }
            }
        }*/

        private void ListaArhitekti_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListView v = sender as ListView;
            IzmenaArhitekte izmenaArhitekte = new IzmenaArhitekte(this, v.SelectedIndex);
            izmenaArhitekte.Show();
        }

        private void ListaDela_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListView v = sender as ListView;
            IzmenaDela izmenaDela = new IzmenaDela(this, v.SelectedIndex);
            izmenaDela.Show();
        }

		private void MenuItem_Click_4(object sender, RoutedEventArgs e)
		{
			TabelaArhitekti ta = new TabelaArhitekti(this);
			ta.Show();
		}

		private void MenuItem_Click_5(object sender, RoutedEventArgs e)
		{
			TabelaDela td = new TabelaDela(this);
			td.Show();
		}

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement focusedControl = Mouse.DirectlyOver;
            if (focusedControl is DependencyObject)
            {
                string str = HelpProvider.GetHelpKey((DependencyObject)focusedControl);
                HelpProvider.ShowHelp(str, this);
            }
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            DemoHelp tutorial = new DemoHelp();
            tutorial.Show();
        }
    }

}
