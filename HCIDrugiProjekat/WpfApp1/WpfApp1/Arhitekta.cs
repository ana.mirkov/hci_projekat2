﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WpfApp1
{
	public class Arhitekta : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string name)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}
        [NonSerialized]
        private WriteableBitmap _slicica;
        public WriteableBitmap Slicica
        {
            get
            {
                return _slicica;
            }
            set
            {
                _slicica = value;
                OnPropertyChanged("Slicica");
            }
        }

        private string idArhitekte;
		private string _name;
		private string _surname;
		private DateTime datumRodjenja;
		private DateTime datumSmrti;
		private string mestoRodjenja;
		private string nacionalnost;
		private string slikaArhitekte;
		private string imePrezime;
        private Dictionary<string, int> xPoint;
        private Dictionary<string, int> yPoint;
        public bool Filter { get; set; }
        public string Name { get => _name; set => _name = value;  }
		public string Surname { get => _surname; set => _surname = value; }
		public string IdArhitekte { get => idArhitekte; set => idArhitekte = value; }
		public DateTime DatumRodjenja { get => datumRodjenja; set => datumRodjenja = value; }
		public DateTime DatumSmrti { get => datumSmrti; set => datumSmrti = value; }
		public string MestoRodjenja { get => mestoRodjenja; set => mestoRodjenja = value; }
		public string Nacionalnost { get => nacionalnost; set => nacionalnost = value; }
		public string SlikaArhitekte { get => slikaArhitekte; set => slikaArhitekte = value; }
        public Dictionary<string, int> XPoint { get => xPoint; set => xPoint = value; }
        public Dictionary<string, int> YPoint { get => yPoint; set => yPoint = value; }

		public string ImePrezime { get => imePrezime; set => imePrezime = value; }

		public Arhitekta(string id, string n, string s, DateTime dr, DateTime ds, string mr, string nac, string sa)
		{
			idArhitekte = id;
			_name = n;
			_surname = s;
			datumRodjenja = dr;
			datumSmrti = ds;
			mestoRodjenja = mr;
			nacionalnost = nac;
			slikaArhitekte = sa;
			imePrezime = n + s;
		}
    }
}
