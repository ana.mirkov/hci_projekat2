﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using Newtonsoft.Json;
using System.IO;
using System.Collections.ObjectModel;

namespace WpfApp1
{
	/// <summary>
	/// Interaction logic for IzmenaDela.xaml
	/// </summary>
	public partial class IzmenaDela : Window
    {
        string path1 = @"..\..\Podaci\dela1.txt";
        private int itemIndex;
        public String IconPath { get; set; }
        public String ActiveMap { get; set; }
        public MainWindow Window { get; set; }
        public ObservableCollection<Arhitekta> arhitekte { get; set; }

        public IzmenaDela()
        {
            InitializeComponent();

        }

        public IzmenaDela(MainWindow window, int index)
        {
            Delo delo = window.Dela[index];
            this.itemIndex = index;
            DataContext = this;
            this.ActiveMap = window.ActiveMap;
            this.Window = window;
            InitializeComponent();
            IconPath = "";
            arhitekte = new ObservableCollection<Arhitekta>();
            Naziv.Text = delo.Naziv;
            Mesto.Text = delo.Mesto;
            Opis.Text = delo.Opis;
            GodinaIzgradnje.Text = delo.GodinaIzgradnje;
            //ARHITEKTE FALE JOS

            foreach (Arhitekta a in delo.Arhitekte)
            {
                arhitekte.Add(a);
                Arhitekte.Items.Add(a);
            }
            
            foreach (ComboBoxItem it in Epoha.Items)
            {
                string content = it.Content.ToString();
                content = content.Replace(' ', '_');
                content = content.ToUpper();
                if (content.Equals(delo.Epoha.ToString()))
                {
                    it.IsSelected = true;
                    break;
                }
            }
           
            
            Zasticen.IsChecked = delo.Zasticeno;
            SvetskoCudo.IsChecked = delo.SvetskoCudo;
            Namena.Text = delo.Namena;

        }

        private void BtnUpdateDelo_Click(object sender, RoutedEventArgs e)
        {
            Delo delo = this.Window.Dela[this.itemIndex];
            Epoha epohas = this.Window.Dela[this.itemIndex].Epoha;
            switch (Epoha.Text)
            {
                case "Drevna Mesopotamija":
                    epohas = WpfApp1.Epoha.drevnaMesopotamija;
                    break;
                case "Antika":
                    epohas = WpfApp1.Epoha.antika;
                    break;
                case "Vizantija":
                    epohas = WpfApp1.Epoha.vizantija;
                    break;
                case "Ranohriscanstvo":
                    epohas = WpfApp1.Epoha.ranohriscanstvo;
                    break;
                case "Romanika":
                    epohas = WpfApp1.Epoha.romanika;
                    break;
                case "Gotika":
                    epohas = WpfApp1.Epoha.gotika;
                    break;
                case "Renesansa":
                    epohas = WpfApp1.Epoha.renesansa;
                    break;
                case "Barok":
                    epohas = WpfApp1.Epoha.barok;
                    break;
                case "Klasicizam":
                    epohas = WpfApp1.Epoha.klasicizam;
                    break;
                case "Moderna":
                    epohas = WpfApp1.Epoha.moderna;
                    break;
                case "Postmoderna":
                    epohas = WpfApp1.Epoha.postmoderna;
                    break;
            }

            delo.Naziv = Naziv.Text;
            delo.Mesto = Mesto.Text;
            delo.Opis = Opis.Text;
            delo.GodinaIzgradnje = GodinaIzgradnje.Text;
            delo.Epoha = epohas;
            delo.Zasticeno = Zasticen.IsChecked ?? false;
            delo.SvetskoCudo = SvetskoCudo.IsChecked ?? false;
            delo.Namena = Namena.Text;
            
            //ikonicaaaaaaaaaaaaaaaaaa
            string json = JsonConvert.SerializeObject(this.Window.Dela, Formatting.Indented);

            using (StreamWriter sw = File.CreateText(path1))
            {
                sw.WriteLine(json);
            }
            this.Close();
        }

        private void BtnDeleteDelo_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Da li ste sigurni da zelite da obrisete ovo delo?", "Provera brisanja", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                this.Window.Dela.RemoveAt(this.itemIndex);
                string json = JsonConvert.SerializeObject(this.Window.Dela, Formatting.Indented);

                using (StreamWriter sw = File.CreateText(path1))
                {
                    sw.WriteLine(json);
                }
                this.Close();
            }
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string str = "izmenaDela";
            HelpProvider.ShowHelp(str, Window);
        }
    }
}
